FROM python:3.6.0-alpine

RUN mkdir -p /opt/src

WORKDIR /opt/src
RUN apk update && apk add --no-cache git
RUN git config --global http.sslVerify false

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY ./src /opt/src

EXPOSE 8000

CMD ["gunicorn", "-b=0.0.0.0:8000", "-w=8", "-t=300", "app:app"]
