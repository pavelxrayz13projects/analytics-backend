.. Counters

Счетчики
========

Счетчики событий, разбитые по группам

Поля документа:

.. code-block:: javascript

    {
      "_items": [
        {
          "group": "malware",
          "count": 1
        },
        {
          "group": "policy",
          "count": 2
        },
        ...
      ]
    }


.. http:get:: /counters?aggregate={"$from":{datetime from},"$to":{datetime to}}

    Получить все счетчики групп

    :query from: Дата начала выборки в формате ISO 8601 с милисекундами ``2016-04-14T18:17:40.700Z``
    :query to: Дата окончания в формате ISO 8601 с милисекундами ``2016-04-14T18:17:41.700Z``

    :statuscode 200: no error
    :statuscode 400: Ошибка в сформированном запросе
