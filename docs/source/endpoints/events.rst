.. Events
.. _events:

События
=======

Сработавшие сигнатуры (события) на IDS

Поля документа:

.. code-block:: javascript

    {
      "_id": "56e0689e1d41c8b535000001",
      "_created": "2016-09-03T18:17:40.699000Z",
      "_updated": "2016-09-03T18:17:40.699000Z",
      "_etag": "88a81a233b1a9a6ba0d9dd7035221ae03c9f88ea",
      "_links": {
        "self": {
          "title": "Event",
          "href": "events/56e0689e1d41c8b535000001"
        },
        "parent": {
          "title": "home",
          "href": "/"
        },
        "collection": {
          "title": "events",
          "href": "events"
        }
      },
      "sensor_ip": "10.0.14.143",   // IP-адрес сенсора
      "sensor_id": "1231231",   // ID сенсора
      "logdate": "2016-09-03T18:17:40.699000Z",     // Время события
      "rule": "1:2000419",   // Идентификатор правила
      "group": "info"       // Группа событий
      "priority": 1,        // Критичность события
      "event_id": "7196276",    // Идентификатор события
      "src": "217.67.177.162",  // IP-адрес источника
      "spt": 80,            // Порт источника
      "dst": "91.244.183.251",      // Адрес получателя события
      "dpt": 65423,         // Порт назначения
      "proto": "TCP",   // Сетевой протокол по которому сработала сигнатура
      "sig_text": "ET POLICY PE EXE or DLL Windows file download",  // Название сработавшей сигнатуры
      "pcap": "/pcap/1249902842/29924151"  // Ссылка для загрузки pcap файла
      "homenet_address": [  // Адреса из домашней сети
        "91.244.183.29"
      ]
    }

.. http:get:: /events/{_id}

    Получить событие по id

    :query _id: id записи в базе

    :statuscode 200: Ок
    :statuscode 404: Запись не найдена

.. http:get:: /events/

    Получить список событий

    Этот ресурс всегда выдаёт :ref:`list-meta` в дополнение к объектам.

    :query page: номер страницы
    :query sort: стандартная :ref:`sort`
    :query where: стандартная :ref:`filter`

    :statuscode 200: no error
    :statuscode 400: Ошибка в сформированном запросе
