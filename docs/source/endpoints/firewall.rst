.. Firewall

Firewall
========

События от TMG Firewall

Поля документа:

.. code-block:: javascript

    {
        _id: "571500da1ed75a015867ad06",
        _created: "1970-01-01T00:00:00.000000Z",
        _updated: "1970-01-01T00:00:00.000000Z",
        _etag: "899ff260e1edd8720a05f98e4706dc9f6a1424fa",
        _links: {
          self: {
            href: "firewall/571500da1ed75a015867ad06",
            title: "Firewall"
          }
        },
        servername: "MSK-TMG-04",
        logTime: "2016-04-18T18:43:54.667000Z",
        protocol: "TCP",
        SourceIP: "10.0.14.137",
        SourcePort: 10533,
        DestinationIP: "40.127.182.186",
        DestinationPort: 443,
        OriginalClientIP: "10.0.14.137",
        Action: 7,
        resultcode: -2147004891,
        ApplicationProtocol: "HTTPS",
        bytessent: 454,
        bytesrecvd: 454,
        connectiontime: 93406,
        DestinationName: "-",
        ClientAgent: "Skype.exe:3:6.3",
        sessionid: 8958979,
        NATAddress: "91.244.183.5",
        FwcAppPath: "D:\programs\Skype\Phone\Skype.exe"
    },

.. http:get:: /firewall/{_id}

    Получить событие по id

    :query _id: id записи в базе

    :statuscode 200: Ок
    :statuscode 404: Запись не найдена

.. http:get:: /firewall/

    Получить список событий

    Этот ресурс всегда выдаёт :ref:`list-meta` в дополнение к объектам.

    :query page: номер страницы
    :query sort: стандартная :ref:`sort`
    :query where: стандартная :ref:`filter`

    :statuscode 200: no error
    :statuscode 400: Ошибка в сформированном запросе
