.. Incident counter

Счетчик инцидентов
==================

Получить количество необработанных инцидентов

Поля документа:

.. code-block:: javascript

    {
      "_items": [
        {
          "status": "expected",
          "count": 1
        }
      ]
    }


.. http:get:: /incident_counter?aggregate={"$from":{datetime from},"$to":{datetime to}}

    Получить количество необработанных инцидентов

    :query from: Дата начала выборки в формате ISO 8601 с милисекундами ``2016-04-14T18:17:40.700Z``
    :query to: Дата окончания в формате ISO 8601 с милисекундами ``2016-04-14T18:17:41.700Z``

    :statuscode 200: no error
    :statuscode 400: Ошибка в сформированном запросе
