.. Incidents
.. _incidents:

Инциденты
=========

Зафиксированные инциденты информационной безопасности

Поля документа:

.. code-block:: javascript

    {
        {
            "location": { //сведения где произошел инцидент (обязательное)
                "organization": "ЗАО ПМ",
                "branch": "Основной офис",
                "uuid": "32323-34532423-234234-54544", //уникальный идентификатор (обязательное)

            },
            "title": "Заражение хоста трояном Kazy", //краткое описание (обязательное)
            "level": "10",     //уровень критичности от 1 до 10 (целое значение. 9-10 - критичный(подтвержденный инцидент), 7-8 - высокий уровень(вероятность инцидента), 5-6 - средний уровень (предполагаемый инцидент), < 5 - низкий уровень) (обязательное)
            "mode": "meta",  //механизм генерации карточки. meta - на основе метаправил, manual - вручную, classify - математикой (обязательное)
            "symptoms": [       //симптомы (признаки инцидента) (обязательное)
                "Аномальная сетевая активность"
            ],
            "threats": [        //угрозы
                "Нарушение конфиденциальности"
            ],
            "methods": [        //методы реализации угроз (классы)
                "Вредоносное ПО"
            ],
            "created": "2015-03-23T08:00:00.000Z",  //дата создания карточки обязательно в UTC (обязательное)
            "status": "inwork",  // Статус инцидента
            "beginprocessing": "2015-03-23T08:00:00.000Z",  //дата начала обработки
            "endprocessing": "2015-03-23T08:00:00.000Z",  //дата окончания обработки
            "affected_actives": [   //пораженные активы
                {
                    "ip": "91.244.183.74",
                    "user": "workspace\\pumnikov",
                    "mac": "54-04-A6-D2-24-56",
                    "host_name": ""
                }
            ],
            "recommendations": [    //рекомендации
                "Отключить зараженный компьютер от сети",
                "Провести интервьюирование владельца",
                "Осуществить антивирусную проверку",
                "Передать обнаруженное вредоносное ПО в ЦМ для анализа",
                "Удалить обнаруженное вредоносное ПО"
            ],
            "main_events": [    //основные события, иницировавшие инцидент (обязательное)
                {
                    "sensor": "IDS",    //тип сенсора IDS, PROXY_TMG, FIREWALL_TMG, HIDS (обязательное)
                    "segment": "Внутренний сегмент",
                    "external_id": "33223443",  //идентификатор IDS (обязательное)
                    "ip": "10.0.14.80",   //IP адрес сенсора
                    "events": [ //события с данного сенсора (обязательное)
                        {
                            "date_fact": "2015-03-23T07:58:29.000Z", //в UTC
                            "message": "AM MALWARE KAZY",
                            "src_address": "21.242.183.11:80",
                            "src_mac": "54-04-A6-D2-24-56",
                            "dst_address": "91.244.183.74:51098",
                            "dst_mac": "76-01-B2-D2-21-74",
                            "pcap_link": "10.0.14.80/service/pcap_payload/11111",
                            "event_id": "11111"
                        }
                    ]
                }
            ],
            "correlated_events": [  //кореллированные события
                {
                    "sensor": "IDS", (обязательное)
                    "segment": "Внутренний сегмент",
                    "external_id": "33223443", (обязательное)
                    "ip": "10.0.14.80",
                    "events": [
                        {
                            "date_fact": "2015-03-23T07:58:29.000Z",
                            "message": "ET POLICY PE EXE or DLL Windows file download",
                            "src_address": "21.242.183.11:80",
                            "src_mac": "54-04-A6-D2-24-56",
                            "dst_address": "91.244.183.74:51098",
                            "dst_mac": "76-01-B2-D2-21-74",
                            "pcap_link": "10.0.14.80/service/pcap_payload/11102",
                            "event_id": 11102
                        }
                    ]

                },
                {
                    "sensor": "PROXY_TMG", (обязательное)
                    "segment": "Внутренний сегмент",
                    "external_id": "1234223", (обязательное)
                    "ip": "",
                    "events": [
                        {
                            "time": "24.02.2016 0:00",
                            "clientip": "10.0.9.67",
                            "srcport": 52492,
                            "destip": "149.154.167.51",
                            "destport": 80,
                            "desthost": "149.154.167.51",
                            "protocol": "http",
                            "operation": "POST",
                            "uri": "http://149.154.167.51:80/api",
                            "mimetype": "application/octet-stream",
                            "resultcode": 200,
                            "bytesrecvd": 573,
                            "bytessent": 350,
                            "clientagent": "Mozilla/5.0",
                            "processingtime": 78
                        },
                        {
                            "time": "24.02.2016 0:05",
                            "clientip": "10.0.9.67",
                            "srcport": 52493,
                            "destip": "149.154.167.51",
                            "destport": 80,
                            "desthost": "149.154.167.51",
                            "protocol": "http",
                            "operation": "POST",
                            "uri": "http://149.154.167.51:80/api/22",
                            "mimetype": "application/octet-stream",
                            "resultcode": 200,
                            "bytesrecvd": 573,
                            "bytessent": 350,
                            "clientagent": "Mozilla/5.0",
                            "processingtime": 78
                        }
                    ]
                },
                {
                    "sensor": "FIREWALL_TMG", (обязательное)
                    "segment": "Внутренний сегмент",
                    "external_id": "3453452", (обязательное)
                    "ip": "",
                    "events": [
                        {
                            "servername": "MSK-TMG-04",
                            "time": "2016-02-25 15:00:00.013",
                            "protocol": "TCP",
                            "srcip": "91.244.183.5",
                            "srcport": 18581,
                            "dstip": "217.69.141.113",
                            "dstport": 443,
                            "clientip": "91.244.183.5",
                            "action": 7,
                            "rule":"",
                            "applicationprotocol": "HTTPS",
                            "bytessent": 20697,
                            "bytesrecvd": 46557,
                            "connectiontime": 0,
                            "dstname": "",
                            "clientusername": "",
                            "clientagent": "",
                            "sessionid": 7322348,
                            "nataddress": "0.0.0.0",
                            "fwcapppath": ""
                        }
                    ]
                }
            ],
            "summary": "Выявлена активность трояна Kazy. Троян загружает на компьютер жертвы дополнительно ПО и вносит изменения в реестр системы."     //общее описание (обязательное)
        }
    }

.. http:get:: /incidents/{_id}

    Получить инцидент по id

    :query event_id: id записи в базе

    :statuscode 200: Ок
    :statuscode 404: Запись не найдена

.. http:get:: /incidents/

    Получить список инцидентов

    Этот ресурс всегда выдаёт :ref:`list-meta` в дополнение к объектам.

    :query page: номер страницы
    :query sort: стандартная :ref:`sort`
    :query where: стандартная :ref:`filter`. Если необходим фильтр ко вложенным полям, то работает обращение через '.'

    .. code-block:: none

       Пример:
       http://10.0.9.118:8000/incidents?where={"main_events.ip":"10.0.9.80"} - Для получения инцидентов на одном сенсоре
       http://10.0.9.118:8000/incidents?where={"main_events.ip":{"$in":["10.0.9.80","10.0.9.81"]} - Для получения инцидентов на нескольких сенсорах

    :statuscode 200: no error
    :statuscode 400: Ошибка в сформированном запросе

.. http:patch:: /incidents/{_id}

    Изменить статус инцидента
    Тело запроса аналогично POST. В нём передавать status={_status}. Варианты статусов - в meta choices от /incidents/ (общего списка) т.к. к штучному ответу метаданные не прикрепляются

    :statuscode 200: Ок
    :statuscode 400: Ошибка в сформированном запросе
