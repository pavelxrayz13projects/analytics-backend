.. Rules
.. _rules:

Правила
=======

Исходные тексти правил БРП

Поля документа:

.. code-block:: javascript

    {
      "_id": "1:3000038",  // gid:sid
      "text": "ET POLICY PE EXE or DLL Windows file download ...",  // Исходный текст сигнатуры
    }

.. http:get:: /rules/{_id}

    Получить правило по id

    :query _id: id записи в базе

    :statuscode 200: Ок
    :statuscode 404: Запись не найдена

.. http:get:: /rules/

    Получить список правил

    Этот ресурс всегда выдаёт :ref:`list-meta` в дополнение к объектам.

    :query page: номер страницы
    :query sort: стандартная :ref:`sort`
    :query where: стандартная :ref:`filter`

    :statuscode 200: no error
    :statuscode 400: Ошибка в сформированном запросе
