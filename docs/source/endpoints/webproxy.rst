.. Web Proxy

Web Proxy
=========

События от TMG Web Proxy

Поля документа:

.. code-block:: javascript

    {
        _id: "57146be21ed75a7945e3d36c",
        _created: "1970-01-01T00:00:00.000000Z",
        _updated: "1970-01-01T00:00:00.000000Z",
        _etag: "40de5ae5786bf0db46b79d9177d1167a0fa6b714",
        _links: {
          self: {
            href: "webproxy/57146be21ed75a7945e3d36c",
            title: "Webproxy"
          }
        },
        servername: "MSK-TMG-04",
        logTime: "2016-04-18T08:08:42.750000Z",
        ClientIP: "10.0.14.68",
        SrcPort: 27408,
        DestHostIP: "91.137.180.93",
        DestHostPort: 443,
        DestHost: "91.137.180.93",
        protocol: "SSL-tunnel",
        operation: "-",
        uri: "91.137.180.93:443",
        mimetype: "-",
        resultcode: 0,
        bytesrecvd: 164,
        bytessent: 277,
        ClientAgent: "-",
        processingtime: 0,
        transport: "TCP",
        referredserver: "-"
    },

.. http:get:: /webproxy/{_id}

    Получить событие по id

    :query _id: id записи в базе

    :statuscode 200: Ок
    :statuscode 404: Запись не найдена

.. http:get:: /webproxy/

    Получить список событий

    Этот ресурс всегда выдаёт :ref:`list-meta` в дополнение к объектам.

    :query page: номер страницы
    :query sort: стандартная :ref:`sort`
    :query where: стандартная :ref:`filter`

    :statuscode 200: no error
    :statuscode 400: Ошибка в сформированном запросе
