.. Analytics Backend documentation master file, created by
   sphinx-quickstart on Mon Apr  4 18:58:22 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Документация Analytics Backend
==============================
Содержание:

.. toctree::
    :maxdepth: 2

    endpoints/index
    general


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
