# Поля из лога IDS 2.4.1

Из лога берутся не все поля. Вот список того что будет в коллекции логов:

| название | тип | описание |
|---|---|---|
| sensor_ip | string | IP-адрес сенсора |
| ids_version | string | Версия ViPNet IDS |
| logdate | date | Время события |
| gid | string | GID правила |
| sid | string | SID правила |
| rev | string | Ревизия правила |
| priority | integer | Важность атаки (может принимать значения от 1 до 3) |
| event_id | string | Идентификатор события на сенсоре |
| sensor_id | string | Идентификатор сенсора
| src | string | IP-адрес источника |
| spt | int | Порт источника |
| smac | string | MAC-адрес источника |
| dst | string | IP-адрес назначения |
| dpt | int | Порт назначения |
| dmac | string | MAC-адрес назначения |
| proto | string | Протокол |
| cnt | int | Число событий в агрегированном событии |
| sig_text | string | Сообщение сигнатуры |
| tags | string array | Метки правил |


# Детали сервиса

## Фильтры, сортировки, страницы

В примерах нужно экранировать кавычки (%22) и пробелы (%20), тут всё стандартно для url

### Filtering

[Filtering](http://python-eve.org/features.html#filtering)

Простые примеры:

`curl -i http://eve-demo.herokuapp.com/people?where=lastname=="Doe"`

или

`http://eve-demo.herokuapp.com/people?where={"lastname": "Doe"}`

Вот посложнее:

`$ curl -g -X GET -H "Accept: application/json; charset=utf-8" -H "Cache-Control: no-cache" 'http://{your_host}.mybluemix.net/api/v1/mac?where={"organization":{"$regex":"^ibm.*?$","$options":"i"}}'`

``curl -i http://eve-demo.herokuapp.com/people?where={"qnty": {"$lte": 10}}``

### Sorting

[Sorting](http://python-eve.org/features.html#sorting)

Можно так: `curl -i http://eve-demo.herokuapp.com/people?sort=city,-lastname`

Или так: `http://eve-demo.herokuapp.com/people?sort=[("lastname", -1)]`

### Pagination

[Pagination](http://python-eve.org/features.html#pagination)

Вот: `curl -i http://eve-demo.herokuapp.com/people?max_results=20&page=2`

В сервисе будет max_results по умолчанию и ограничено максимальное значение


# Страницы сервиса

## События ids

Описание объекта:

|Поле|Тип|Описание|
|----|---|--------|
| logdate | date | Время срабатывания сигнатуры |
| sensor_ip | string | Адрес IDS на которой сработала сигнатура |
| src | ip | Адрес источника события |
| spt | int | Порт источника |
| dst | ip | Адрес получателя события |
| dpt | int | Порт получателя |
| sig_text | string | Название сработавшей сигнатуры |
| event_id | str | Идентификатор события |
| proto | str | Сетевой протокол по которому сработала сигнатура |
| priority | int | Критичность события |
| pcap | url | Ссылка для выгрузки pcap-файла |



resource: `/events`

```json
{
  "_items": [
    {
      "sid": "2012600",
      "priority": 2,
      "_id": "56d9d99738758c7649000001",
      "_created": "Thu, 01 Jan 1970 00:00:00 GMT",
      "sig_text": "ET POLICY Suspicious inbound to MSSQL port 1433",
      "gid": "1",
      "event_id": "106868",
      "proto": "TCP",
      "pcap": "http://127.0.0.1/service/pcap_payload?id=106868",
      "_etag": "450f221c2eb588f6b9afe0600333a51224ef68ac",
      "logdate": "Fri, 04 Mar 2016 15:25:18 GMT",
      "sensor_ip": "127.0.0.1",
      "dpt": 1433,
      "spt": 6000,
      "_links": {
        "self": {
          "title": "Event",
          "href": "events/56d9d99738758c7649000001"
        }
      },
      "_updated": "Thu, 01 Jan 1970 00:00:00 GMT",
      "dst": "81.30.214.98",
      "src": "61.160.236.22"
    },
    {
      "sid": "2012600",
      "priority": 2,
      "_id": "56d9d99838758ca9d4000002",
      "_created": "Thu, 01 Jan 1970 00:00:00 GMT",
      "sig_text": "ET POLICY Suspicious inbound to MSSQL port 1433",
      "gid": "1",
      "event_id": "106868",
      "proto": "TCP",
      "pcap": "http://127.0.0.1/service/pcap_payload?id=106868",
      "_etag": "8a1d2fecba7b4433479814d1989dc1e99c31a320",
      "logdate": "Fri, 04 Mar 2016 15:25:18 GMT",
      "sensor_ip": "127.0.0.1",
      "dpt": 1433,
      "spt": 6000,
      "_links": {
        "self": {
          "title": "Event",
          "href": "events/56d9d99838758ca9d4000002"
        }
      },
      "_updated": "Thu, 01 Jan 1970 00:00:00 GMT",
      "dst": "81.30.214.98",
      "src": "61.160.236.22"
    }
  ],
  "_links": {
    "self": {
      "title": "events",
      "href": "events"
    },
    "parent": {
      "title": "home",
      "href": "/"
    }
  },
  "_meta": {
    "total": 2,
    "page": 1,
    "max_results": 25
  }
```

item: `/evenrs/{id}`

```json
{
  "sid": "2012600",
  "priority": 2,
  "_id": "56d9d99738758c7649000001",
  "_created": "Thu, 01 Jan 1970 00:00:00 GMT",
  "sig_text": "ET POLICY Suspicious inbound to MSSQL port 1433",
  "gid": "1",
  "event_id": "106868",
  "proto": "TCP",
  "pcap": "http://127.0.0.1/service/pcap_payload?id=106868",
  "_etag": "450f221c2eb588f6b9afe0600333a51224ef68ac",
  "logdate": "Fri, 04 Mar 2016 15:25:18 GMT",
  "sensor_ip": "127.0.0.1",
  "dpt": 1433,
  "spt": 6000,
  "_links": {
    "self": {
      "title": "Event",
      "href": "events/56d9d99738758c7649000001"
    },
    "parent": {
      "title": "home",
      "href": "/"
    },
    "collection": {
      "title": "events",
      "href": "events"
    }
  },
  "_updated": "Thu, 01 Jan 1970 00:00:00 GMT",
  "dst": "81.30.214.98",
  "src": "61.160.236.22"
}
```
