from pymongo import MongoClient


groups = [
    {'_id': 'info', 'tags': ['info']},
    {'_id': 'scan', 'tags': ['scan', 'scan_attempt', 'scan_response']},
    {'_id': 'auth', 'tags': ['password', 'login_attempt', 'login_response']},
    {'_id': 'policy', 'tags': ['policy']},
    {'_id': 'malware', 'tags': ['malware']},
    {'_id': 'ddos', 'tags': ['ddos_response', 'ddos_attempt']},
    {'_id': 'attacks', 'tags': ['attack_response', 'exploitation_response',
                                'exploitation_attempt', 'drive-by',
                                'sql_injection', 'dos', 'dos_attempt',
                                'dos_response']},
    {'_id': 'other', 'tags': ['spam', 'other_suspicious']},
]

client = MongoClient('localhost', 27017)
db = client.tias

collection = db.groups

result = collection.insert_many(groups)
