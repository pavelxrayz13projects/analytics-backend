from bson.json_util import loads
from pymongo import MongoClient


logs_text = open('../fixtures/analytic_logs.json').read()

logs = loads(logs_text)

client = MongoClient('localhost', 27017)
db = client.tias

collection = db.analytic_logs

result = collection.insert_many(logs)
