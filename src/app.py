import os

from eve import Eve

from hooks.event import (before_returning_events, before_returning_event)
from hooks.counter import (before_aggregation_counters,
                           before_returning_counters)
from hooks.fired_signature import (before_returning_fired_signatures,
                                   before_aggregation_fired_signatures_dst,
                                   before_aggregation_fired_signatures_src)
from hooks.general import add_choices
from hooks.histogram import (before_aggregation_histogram,
                             before_returning_histogram)
from hooks.incident_counter import (before_aggregation_incidnet_counter,
                                    before_returning_incident_counter)
from hooks.incidents import (before_incident_update, before_returning_incident,
                             before_returning_incidents)


# Создадим приложение
app = Eve(settings='/opt/src/settings.py')
# Добавим триггеры

# Events
app.on_fetched_resource_events += before_returning_events
app.on_fetched_item_events += before_returning_event

# Counters
app.pre_aggregation_counters += before_aggregation_counters
app.on_fetched_aggregation_counters += before_returning_counters

# Fired Signatures
app.on_fetched_aggregation_fired_signatures_dst += before_returning_fired_signatures
app.pre_aggregation_fired_signatures_dst += before_aggregation_fired_signatures_dst
app.on_fetched_aggregation_fired_signatures_src += before_returning_fired_signatures
app.pre_aggregation_fired_signatures_src += before_aggregation_fired_signatures_src

# Events Histogram
app.pre_aggregation_events_histogram += before_aggregation_histogram
app.on_fetched_aggregation_events_histogram += before_returning_histogram

# Incidents Histogram
app.pre_aggregation_incidents_histogram += before_aggregation_histogram
app.on_fetched_aggregation_incidents_histogram += before_returning_histogram

# Incident Counter
app.pre_aggregation_incident_counter += before_aggregation_incidnet_counter
app.on_fetched_aggregation_incident_counter += before_returning_incident_counter

# Incidents
app.on_update_incidents += before_incident_update
app.on_fetched_resource_incidents += before_returning_incidents
app.on_fetched_item_incidents += before_returning_incident

# general triggers
app.on_fetched_resource += add_choices
app.on_fetched_aggregation += add_choices

# Запустим приложение
if __name__ == '__main__':
    app.run(host=os.getenv('BACKEND_HOST', '0.0.0.0'),
            port=int(os.getenv('BACKEND_PORT', 8000)))
