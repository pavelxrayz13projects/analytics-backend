"Общие константы приложения"


GROUPS = [
    'info',
    'scan',
    'auth',
    'policy',
    'malware',
    'ddos',
    'attacks',
    'other',
]


EXPECTED_INCIDENT = 'expected'
IN_WORK = 'inwork'
CLOSED = 'closed'
ALLOWED_INCIDENT_STATUS = [
    EXPECTED_INCIDENT,
    IN_WORK,
    'notconfirmed',
    'confirmed',
    CLOSED,
]

INCIDENT_SENSOR_FIELD = 'main_events.ip'

CHOICES = {
    'events': {  # Название ресурса, внутри - поля с вариантами
        'proto': ['TCP', 'UDP', 'ICMP'],
        'priority': [1, 2, 3],
        'group': GROUPS,
        'sensor_ip': None,  # Заполняется в хуке общих триггеров
    },
    'fired_signatures_src': {
        'priority': [1, 2, 3],
        'proto': ['TCP', 'UDP', 'ICMP'],
        'group': GROUPS,
        'sensor_ip': None,  # Заполняется в хуке общих триггеров
    },
    'fired_signatures_dst': {
        'priority': [1, 2, 3],
        'proto': ['TCP', 'UDP', 'ICMP'],
        'group': GROUPS,
        'sensor_ip': None,  # Заполняется в хуке общих триггеров
    },
    'incidents': {
        'status': ALLOWED_INCIDENT_STATUS,
    },
}
