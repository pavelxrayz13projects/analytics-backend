"""Вспомогательные утилиты
"""
from collections import OrderedDict

import calendar
from datetime import datetime, timedelta

from eve import RFC1123_DATE_FORMAT
from pymongo import MongoClient

import settings


def create_pcap_url(sensor_id, event_id):
    """
    Сформировать ссылку для загрузки pcap
    :param host: адрес IDS
    :param event_id: ID события на IDS
    :return: ссылка на загрузку pcap файла
    """
    url = '/pcap/{}/{}'.format(sensor_id, event_id)
    return url


def str_to_date(string):
    """
    Преобразование строки в datetime объект,
    в соответствии с форматом из настроек

    :param string: строка с датой и временем
    :return: datetime
    """
    date_format = getattr(settings, 'DATE_FORMAT', RFC1123_DATE_FORMAT)

    try:
        parsed_date = datetime.strptime(string, date_format)
    except (ValueError, TypeError):
        return None

    return parsed_date


def get_sensors():
    """
    Получить список всех сенсоров
    """
    addresses = []
    client = MongoClient(settings.MONGO_HOST)
    collection = client.tias.organisations
    organisations = collection.find({}, {'branches.segments.sensors.address': 1})
    for organisation in organisations:
        for branch in organisation['branches']:
            for segment in branch['segments']:
                for sensor in segment['sensors']:
                    addresses.append(sensor['address'])
    return addresses


def get_timestep_key(data):
    """
    Преобразовать дату и время в формат ключа для шага
    :param data:
    :return: '2016-151-17-21'
    """
    result = '{0}-{1:03d}-{2:02d}-{3:02d}'.format(
        data.year,
        data.timetuple().tm_yday,
        data.hour,
        data.minute,
    )
    return result


def create_time_range_with_step(starts, ends, step):
    """
    Создать упорядоченынй dict для временного промежутка с шагами
    :param starts: с
    :param ends: по
    :param step: шаг
    :return:
    """
    result_range = []
    step = int(step / 1000)
    step_delta = timedelta(seconds=step)

    starts_timestamp = calendar.timegm(starts.timetuple())

    current_step = starts - timedelta(seconds=(starts_timestamp % step))

    while current_step <= ends:
        result_range.append((get_timestep_key(current_step), 0))
        current_step += step_delta
    return OrderedDict(result_range)


def create_datetime_key(timestamp):
    """
    Создать ключ по элементу агрегации
    :return:
    """
    step_key = datetime.utcfromtimestamp(timestamp/1000)
    key = step_key.strftime('%Y-%j-%H-%M')
    return key
