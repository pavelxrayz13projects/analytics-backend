"""Триггеры для счетчиков событий
"""
import json

from flask import abort

from helpers.utils import str_to_date
from models.event import schema as events_fields


def before_aggregation_counters(request, pipeline):
    """
    Подготовка агрегированной выборки в базу
    :param request: параметры запроса
    :param pipeline: выборка в базу
    :return:
    """
    try:
        params = json.loads(request.aggregation)
    except (ValueError, TypeError):
        params = {}

    # Получим и преобразуем параметры в даты
    starts = str_to_date(params.get('$from'))
    ends = str_to_date(params.get('$to'))

    if not starts or not ends:
        abort(400, description='Aggregation query needs two params: from & to')

    # Изменим запрос
    filter_stage = {"logdate": {"$gte": starts, "$lte": ends}}

    # Применим все фильтры к коллекции логов
    for item, value in params.items():
        if item in events_fields:
            filter_stage[item] = value

    pipeline[0]['$match'] = filter_stage

    return pipeline


def before_returning_counters(response, *args):
    """
    Для отображение оставим только название группы и счетчик
    :param response:
    :return:
    """
    for item in response['_items']:
        group = item.get('_id').get('group')
        # _id нам больше не нужен
        del item['_id']

        # Если без группы - пропускаем
        if not group:
            item['group'] = 'no_group'
            continue

        item['group'] = group

    return response
