"""Обработка данных пришедших из базы перед остылкой клиенту
"""
from helpers.utils import create_pcap_url


def before_returning_events(response):
    # добавим дополнительные поля
    for item in response['_items']:
        sensor_id = item.get('sensor_id')
        event_id = item.get('event_id')
        item['pcap'] = create_pcap_url(sensor_id, event_id)

    return response


def before_returning_event(response):
    # добавим дополнительные поля
    sensor_id = response.get('sensor_id')
    event_id = response.get('event_id')
    response['pcap'] = create_pcap_url(sensor_id, event_id)

    return response
