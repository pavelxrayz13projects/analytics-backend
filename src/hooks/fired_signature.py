"""Обработка данных пришедших из базы перед остылкой клиенту
"""
import json

from flask import abort

from helpers.utils import str_to_date
from models.event import schema as events_fields


def before_returning_fired_signatures(response, *args):
    """
    Для отображение уберём id
    :param response:
    :return:
    """
    for item in response['_items']:
        # _id нам не нужен
        del item['_id']
        # Проставим поле homenet
        address = item['address']
        item['address'] = {
            'ip': address
        }
        if item['_homenet'] and address in item['_homenet']:
            item['address']['in_homenet'] = True
        else:
            item['address']['in_homenet'] = False

        del item['_homenet']

    return response


def before_aggregation_fired_signatures_dst(request, pipeline):
    pipeline = before_aggregation_fired_signatures('dst', request, pipeline)
    return pipeline


def before_aggregation_fired_signatures_src(request, pipeline):
    pipeline = before_aggregation_fired_signatures('src', request, pipeline)
    return pipeline


def before_aggregation_fired_signatures(direction, request, pipeline):
    """
    Подготовка агрегированной выборки в базу
    :param request: параметры запроса
    :param pipeline: выборка в базу
    :return:
    """
    try:
        params = json.loads(request.aggregation)
    except (ValueError, TypeError):
        params = {}

    # Получим и преобразуем параметры в даты
    starts = str_to_date(params.get('$from'))
    ends = str_to_date(params.get('$to'))
    address = params.get('address')

    if not starts or not ends:
        abort(400, description='Aggregation query needs two params: from & to')

    filter_stage = {'logdate': {'$gte': starts, '$lte': ends}}

    # Применим все фильтры к коллекции логов
    for item, value in params.items():
        if item in events_fields:
            filter_stage[item] = value
    if address:
        filter_stage[direction] = address

    pipeline[0]['$match'] = filter_stage

    # Данные поля мы получаем на разных этапах агрегации, применим их по порядку
    stages = (
        ('count', 2),
    )

    for key, position in stages:
        parameter = params.get(key)
        stage = {key: parameter} if parameter else {}
        pipeline[position]['$match'] = stage

    # Применим сортировку
    sort = params.get('$sort')
    if sort:
        pipeline[-1]['$sort'] = sort

    return pipeline
