"""Общие триггеры"""
import constants
from helpers.utils import get_sensors


def add_choices(resource, response, *args):
    """
    Добавим в метаданные информацию о полях с выборами
    :param resource:
    :param response:
    :return:
    """
    choices = constants.CHOICES.get(resource)

    if not choices:
        return response

    if '_meta' not in response:
        response['_meta'] = {}

    response['_meta']['choices'] = choices
    if 'sensor_ip' in choices:
        response['_meta']['choices']['sensor_ip'] = get_sensors()

    return response
