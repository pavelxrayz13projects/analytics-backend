"""Обработка данных пришедших из базы перед остылкой клиенту
"""
import json
from datetime import timedelta

from flask import abort

import constants
from helpers.utils import (str_to_date, create_time_range_with_step,
                           create_datetime_key)
from models.event import schema as events_fields
from models.incident import schema as incident_fields


def get_params(request):
    """
    Разбор параметров запроса для гистограмм
    :param request:
    :return:
    """
    try:
        params = json.loads(request.aggregation)
    except (ValueError, TypeError):
        params = {}

    # Получим и преобразуем параметры в даты
    starts = str_to_date(params.get('$from'))
    ends = str_to_date(params.get('$to'))

    try:
        step = int(params.get('$step'))*1000*60  # Переведём в миллисекунды
    except (ValueError, TypeError):
        step = None

    # При агрегации округление шло в меньшую сторону и нужно окрглять до шага
    ends = ends + timedelta(milliseconds=(step-1))

    return params, starts, ends, step


def before_returning_histogram(response, request):
    """
    Для отображения возвращаем только массив
    :param response:
    :param request:
    :return:
    """
    params, starts, ends, step = get_params(request)

    if None in (starts, ends, step):
        abort(400, description='Aggregation query needs three params: $from, $to & $step')

    time_range = create_time_range_with_step(starts, ends, step)
    for item in response['_items']:
        key = create_datetime_key(item['_id'])
        time_range[key] = item['count']

    response['_items'] = list(time_range.values())
    return response


def before_aggregation_histogram(request, pipeline):
    """
    Подготовка агрегированной выборки в базу
    :param request: параметры запроса
    :param pipeline: выборка в базу
    :return:
    """
    params, starts, ends, step = get_params(request)

    if not starts or not ends or not step:
        abort(400, description='Aggregation query needs three params: $from, $to & $step')

    datetime_field = list(pipeline[0]['$match'].keys())[0]
    filter_stage = {datetime_field: {'$gte': starts, '$lte': ends}}

    # Применим все фильтры к коллекции логов
    for item, value in params.items():
        if (item in events_fields) \
           or (item in incident_fields)\
           or item == constants.INCIDENT_SENSOR_FIELD:
            filter_stage[item] = value

    pipeline[0]['$match'] = filter_stage
    pipeline[1]['$group']['_id']['$subtract'][1]['$mod'][1] = step

    return pipeline
