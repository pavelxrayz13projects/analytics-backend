"""Триггеры для счетчика необработанных инцидентов
"""
import json

from flask import abort

from helpers.utils import str_to_date


def before_aggregation_incidnet_counter(request, pipeline):
    """
    Подготовка агрегированной выборки в базу
    :param request: параметры запроса
    :param pipeline: выборка в базу
    :return:
    """
    try:
        params = json.loads(request.aggregation)
    except (ValueError, TypeError):
        params = {}

    # Получим и преобразуем параметры в даты
    starts = str_to_date(params.get('$from'))
    ends = str_to_date(params.get('$to'))

    if not starts or not ends:
        abort(400, description='Aggregation query needs two params: from & to')

    # Изменим запрос
    filter_stage = {'created': {'$gte': starts, '$lte': ends}}

    pipeline[0]['$match'] = filter_stage

    return pipeline


def before_returning_incident_counter(response, *args):
    """
    Для отображение оставим только название группы и счетчик
    :param response:
    :return:
    """
    for item in response['_items']:
        status = item.get('_id').get('status')
        # _id нам больше не нужен
        del item['_id']

        item['status'] = status

    return response
