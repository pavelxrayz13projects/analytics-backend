"""Триггеры на изменение инцидента"""
from constants import IN_WORK, CLOSED
from helpers.utils import create_pcap_url


def before_incident_update(updates, original):
    status = updates['status']
    if status == IN_WORK:
        updates['beginprocessing'] = updates['_updated']
    elif status == CLOSED:
        updates['endprocessing'] = updates['_updated']


def replace_pcap_url(sensor_events):
    sensor_id = sensor_events.get('external_id')
    events = sensor_events.get('events', [])

    for i, event in enumerate(events):
        event_id = event.get('event_id')
        events[i]['pcap_link'] = create_pcap_url(sensor_id, event_id)

    sensor_events['events'] = events

    return sensor_events


def before_returning_incidents(response):
    for item in response['_items']:
        for event_categories in ['main_events', 'correlated_events']:
            sensors = item[event_categories]
            for i, sensor in enumerate(sensors):
                item[event_categories][i] = replace_pcap_url(sensor)

    return response


def before_returning_incident(response):
    for event_categories in ['main_events', 'correlated_events']:
        sensors = response[event_categories]
        for i, sensor in enumerate(sensors):
            response[event_categories][i] = replace_pcap_url(sensor)

    return response
