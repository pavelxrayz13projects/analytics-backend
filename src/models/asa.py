"""Модель логов ASA
"""

# Схема с типами данных
schema = {
    'action': {'type': 'string'},
    'device_ip': {'type': 'string'},
    'device_name': {'type': 'string'},
    'drop_rate_current_avg': {'type': 'string'},
    'drop_rate_current_burst': {'type': 'string'},
    'drop_rate_id': {'type': 'string'},
    'drop_rate_max_avg': {'type': 'string'},
    'drop_rate_max_burst': {'type': 'string'},
    'drop_total_count': {'type': 'string'},
    'drop_type': {'type': 'string'},
    'dst_interface': {'type': 'string'},
    'dst_ip': {'type': 'string'},
    'dst_port': {'type': 'string'},
    'dst_url': {'type': 'string'},
    'err_dst_interface': {'type': 'string'},
    'err_dst_ip': {'type': 'string'},
    'err_protocol': {'type': 'string'},
    'err_src_interface': {'type': 'string'},
    'err_src_ip': {'type': 'string'},
    'event_type': {'type': 'string'},
    'icmp_code': {'type': 'string'},
    'icmp_type': {'type': 'string'},
    'interface': {'type': 'string'},
    'logdate': {'type': 'datetime'},
    'protocol': {'type': 'string'},
    'reason': {'type': 'string'},
    'src_interface': {'type': 'string'},
    'src_ip': {'type': 'string'},
    'src_port': {'type': 'string'},
}

# Объект для домена
asa = {
    'schema': schema,
    'datasource': {
        'source': 'asa_logs'
    }
}
