"""Модель для счетчиков (плиток)"""

aggregation = {
    'pipeline': [
        # Фильтр для временного промежутка
        {
            '$match': {}
        },
        {
            '$group': {
                # Ключ группировки
                '_id': {
                    'group': '$group',
                },
                'count': {'$sum': 1},
            },
        },
        {
            '$project': {
                'count': 1,
            }
        },
    ]
}

# Объект для домена
counters = {
    'datasource': {
        'source': 'analytic_logs',
        'aggregation': aggregation
    },
    'pagination': False,
}
