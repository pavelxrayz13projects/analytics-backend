"""Модель логов IDS
"""

# Схема с типами данных
schema = {
    'sensor_ip': {
        'type': 'string'
    },
    'sensor_id': {
        'type': 'string'
    },
    'logdate': {
        'type': 'datetime'
    },
    'rule': {
        'type': 'string'
    },
    'priority': {
        'type': 'integer'
    },
    'event_id': {
        'type': 'string'
    },
    'src': {
        'type': 'string'
    },
    'spt': {
        'type': 'integer'
    },
    'dst': {
        'type': 'string'
    },
    'dpt': {
        'type': 'integer'
    },
    'proto': {
        'type': 'string'
    },
    'sig_text': {
        'type': 'string'
    },
    'group': {
        'type': 'string'
    },
    'homenet_address': {
        'type': 'list'
    },
}

# Объект для домена
events = {
    'schema': schema,
    'datasource': {
        'source': 'analytic_logs'
    }
}
