"""Агрегированные события для гистограммы
"""
from datetime import datetime, timezone

from bson import SON


aggregation = {
    'pipeline': [
        # Первичный фильтр, задаётся пользователем. Сюда идут временные рамки
        {
            '$match': {
                'logdate': {
                }
            }
        },
        # Группировка данных
        {
            '$group': {
                '_id': {
                    '$subtract': [
                        {
                            '$subtract': [
                                '$logdate',
                                datetime(1970, 1, 1, tzinfo=timezone.utc),
                            ]
                        },
                        {
                            '$mod': [
                                {
                                    '$subtract': [
                                        '$logdate',
                                        datetime(1970, 1, 1, tzinfo=timezone.utc),
                                    ]
                                },
                                1000*60*15,
                            ]
                        }
                    ]
                },
                'count': {'$sum': 1},
            },
        },
        {
            '$sort': SON([('_id', 1)]),
        }
    ]
}

# Объект для домена
events_histogram = {
    'datasource': {
        'source': 'analytic_logs',
        'aggregation': aggregation
    },
    'pagination': False,
}
