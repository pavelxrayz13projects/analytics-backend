"""Агрегированные события по сенсору и получателю (получатели)
"""
from bson import SON


aggregation = {
    'pipeline': [
        # Первичный фильтр, задаётся пользователем. Сюда идут временные рамки
        {
            '$match': {}
        },
        # Группировка данных
        {
            '$group': {
                # Ключ группировки
                '_id': {
                    'rule': '$rule',
                    'address':'$dst',
                    'sensor_id': '$sensor_id',
                },
                'address': {'$first': '$dst'},
                'rule': {'$first': '$rule'},
                'sig_text': {'$first': '$sig_text'},
                'priority': {'$first': '$priority'},
                'proto': {'$first': '$proto'},
                'group': {'$first': '$group'},
                'count': {'$sum': 1},
                '_homenet': {'$first': '$homenet_address'},
                'sensor_ip': {'$first': '$sensor_ip'},
            },
        },
        # Здесь уже можно фильтровать по count
        {
            '$match': {}
        },
        # Сортировка (перегружается пользователем)
        {
            '$sort': SON([('count', -1)]),
        },
    ]
}

# Объект для домена
fired_dst = {
    'datasource': {
        'source': 'analytic_logs',
        'aggregation': aggregation
    }
}
