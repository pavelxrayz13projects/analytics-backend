"""Группы событий
"""

# Схема с типами данных
schema = {
    'groups': {
        'type': 'list'
    },
    'id': {
        'type': 'string'
    }
}

# Объект для домена
groups = {
    'schema': schema,
    'additional_lookup': {
        'url': 'regex("[\w-]+")',
        'field': 'id'
    },
    'datasource': {
        'source': 'groups'
    }
}
