"""Модель инцедентов
"""
from constants import ALLOWED_INCIDENT_STATUS


# Схема с типами данных
schema = {
    '_id': {
        'type': 'string'
    },
    'location': {
        'type': 'dict'
    },
    'title': {
        'type': 'string'
    },
    'level': {
        'type': 'integer'
    },
    'mode': {
        'type': 'string'
    },
    'symptoms': {
        'type': 'list'
    },
    'threats': {
        'type': 'list'
    },
    'methods': {
        'type': 'list'
    },
    'created': {
        'type': 'datetime'
    },
    'affected_actives': {
        'type': 'list'
    },
    'recommendations': {
        'type': 'list'
    },
    'main_events': {
        'type': 'list'
    },
    'correlated_events': {
        'type': 'list'
    },
    'summary': {
        'type': 'string'
    },
    'status': {
        'type': 'string',
        'allowed': ALLOWED_INCIDENT_STATUS,
    },
    'beginprocessing': {
        'type': 'datetime'
    },
    'endprocessing': {
        'type': 'datetime'
    },
}

# Объект для домена
incidents = {
    'schema': schema,
    'datasource': {
        'source': 'tickets',
        'default_sort': [('created', -1)]
    },
    'resource_methods': ['GET'],
    'item_methods': ['GET', 'PATCH']
}
