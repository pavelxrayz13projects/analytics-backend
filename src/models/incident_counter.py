"""Модель для счетчиков (необработанные инциденты)"""
from constants import EXPECTED_INCIDENT


aggregation = {
    'pipeline': [
        # Фильтр для временного промежутка
        {
            '$match': {}
        },
        {
            '$match': {'status': EXPECTED_INCIDENT}
        },
        {
            '$group': {
                # Ключ группировки
                '_id': {
                    'status': '$status',
                },
                'count': {'$sum': 1},
            },
        },
        {
            '$project': {
                'count': 1,
            }
        },
    ]
}

# Объект для домена
incident_counter = {
    'datasource': {
        'source': 'tickets',
        'aggregation': aggregation,
    },
    'pagination': False,
}
