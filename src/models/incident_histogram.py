"""Агрегированные инциденты для гистограммы
"""
from datetime import datetime, timezone

from bson import SON


aggregation = {
    'pipeline': [
        # Первичный фильтр, задаётся пользователем. Сюда идут временные рамки
        {
            '$match': {
                'created': {
                }
            }
        },
        # Группировка данных
        {
            '$group': {
                '_id': {
                    '$subtract': [
                        {
                            '$subtract': [
                                '$created',
                                datetime(1970, 1, 1, tzinfo=timezone.utc),
                            ]
                        },
                        {
                            '$mod': [
                                {
                                    '$subtract': [
                                        '$created',
                                        datetime(1970, 1, 1, tzinfo=timezone.utc),
                                    ]
                                },
                                1000*60*15,
                            ]
                        }
                    ]
                },
                'count': {'$sum': 1},
            },
        },
        {
            '$sort': SON([('_id', 1)]),
        }
    ]
}

# Объект для домена
incidents_histogram = {
    'datasource': {
        'source': 'tickets',
        'aggregation': aggregation
    },
    'pagination': False,
}
