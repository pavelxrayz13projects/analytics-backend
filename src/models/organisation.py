"""Модель организаций
"""

# Схема с типами данных
schema = {
    'name': {
        'type': 'string'
    },
    'branches': {
        'type': 'dict'
    },
}

# Объект для домена
organisations = {
    'schema': schema,
    'datasource': {
        'source': 'organisations'
    }
}
