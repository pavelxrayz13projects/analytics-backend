"""Модель правил БРП
"""


# Схема с типами данных
schema = {
    '_id': {
        'type': 'string'
    },
    'text': {
        'type': 'string'
    },
}

# Объект для домена
rules = {
    'schema': schema,
    'datasource': {
        'source': 'rules',
    },
    'item_url': 'regex("[0-9]+:[0-9]+")',
    'resource_methods': ['GET'],
}
