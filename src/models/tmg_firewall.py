#!/usr/bin/env python
# -*- coding: utf-8 -*-
#===============================================================================
"""
    Моделька описывающая Структуру коллекции FirewallLog
"""
#===============================================================================
# Схема с типами данных
schema = {
            'servername':{'type':'string'},

            'logTime':{'type':'datetime'},

            'protocol':{'type':'string'},

            'SourceIP':{'type':'string'},
            
            'SourcePort':{'type':'integer'},
            
            'DestinationIP':{'type':'string'},
            
            'DestinationPort':{'type':'integer'},
            
            'OriginalClientIP':{'type':'string'},
            
            'Action':{'type':'integer'},
            
            'resultcode':{'type':'integer'},
            
            'ApplicationProtocol':{'type':'string'},
            
            'bytessent':{'type':'integer'},
            
            'bytesrecvd':{'type':'integer'},
            
            'connectiontime':{'type':'integer'},
            
            'DestinationName':{'type':'string'},
            
            'ClientUserName':{'type':'string'},
            
            'ClientAgent':{'type':'string'},
            
            'sessionid':{'type':'integer'},
            
            'NATAddress':{'type':'string'},
            
            'FwcAppPath':{'type':'string'},

        }

# Объект для домена
firewall = {
            'schema': schema,
            'datasource': {'source': 'TMG_FirewallLog'}
                        }
