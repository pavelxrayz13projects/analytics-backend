#!/usr/bin/env python
# -*- coding: utf-8 -*-
#===============================================================================
"""
    Моделька описывающая структуру коллекции WebProxyLog
"""
#===============================================================================
# Схема с типами данных
schema = {
            'ClientIP':{'type':'string'},
            
            'ClientUserName':{'type':'string'},
            
            'ClientAgent':{'type':'string'},
            
            'logTime':{'type':'datetime'},
            
            'servername':{'type':'string'},
            
            'referredserver':{'type':'string'},
            
            'DestHost':{'type':'string'},
            
            'DestHostIP':{'type':'string'},
            
            'DestHostPort':{'type':'integer'},
            
            'processingtime':{'type':'integer'},
            
            'bytesrecvd':{'type':'integer'},
            
            'bytessent':{'type':'integer'},
            
            'protocol':{'type':'string'},
            
            'transport':{'type':'string'},
            
            'operation':{'type':'string'},
            
            'uri':{'type':'string'},
            
            'mimetype':{'type':'string'},
            
            'resultcode':{'type':'integer'},
            
            'SrcPort':{'type':'integer'},
        }


# Объект для домена
webproxy = {
            'schema': schema,
            'datasource': {'source': 'TMG_WebProxyLog'}
        }
