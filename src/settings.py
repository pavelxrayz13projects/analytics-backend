"""Файл настроек сервиса
"""
import os

from models.event import events
from models.tmg_webproxy import webproxy
from models.tmg_firewall import firewall
from models.counter import counters
from models.incident import incidents
from models.organisation import organisations
from models.group import groups
from models.event_histogram import events_histogram
from models.incident_histogram import incidents_histogram
from models.fired_signatures_dst import fired_dst
from models.fired_signatures_src import fired_src
from models.incident_counter import incident_counter
from models.rules import rules
from models.asa import asa


MONGO_HOST = os.getenv('MONGO_HOST', '127.0.0.1')
MONGO_PORT = int(os.getenv('MONGO_PORT', '27017'))
MONGO_DBNAME = os.getenv('MONGO_DBNAME', 'tias')

DEBUG = os.getenv('DEBUG', False)

XML = os.getenv('XML', False)
ENSURE_ASCII = False

DATE_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'

MONGO_QUERY_BLACKLIST = []

IF_MATCH = False

DOMAIN = {
    'events': events,
    'firewall': firewall,
    'fired_signatures_dst': fired_dst,
    'fired_signatures_src': fired_src,
    'webproxy': webproxy,
    'counters': counters,
    'incidents': incidents,
    'organisations': organisations,
    'groups': groups,
    'events_histogram': events_histogram,
    'incidents_histogram': incidents_histogram,
    'incident_counter': incident_counter,
    'rules': rules,
    'asa': asa,
}
