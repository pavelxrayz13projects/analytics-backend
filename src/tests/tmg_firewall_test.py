#!/usr/bin/env python
# -*- coding: utf-8 -*-
#===============================================================================
"""
    Юнит тест дляТест_для_модельки_tmg_firewall
"""
#===============================================================================
import os, sys
sys.path.append(os.getcwd())

import unittest
import random
from models.tmg_firewall import schema, firewall

class  TestFirwall(unittest.TestCase):
    def setUp(self):
        self.schema = {
            'servername':{'type':'string'},

            'logTime':{'type':'datetime'},

            'protocol':{'type':'string'},

            'SourceIP':{'type':'string'},
            
            'SourcePort':{'type':'integer'},
            
            'DestinationIP':{'type':'string'},
            
            'DestinationPort':{'type':'integer'},
            
            'OriginalClientIP':{'type':'string'},
            
            'Action':{'type':'integer'},
            
            'resultcode':{'type':'integer'},
            
            'ApplicationProtocol':{'type':'string'},
            
            'bytessent':{'type':'integer'},
            
            'bytesrecvd':{'type':'integer'},
            
            'connectiontime':{'type':'integer'},
            
            'DestinationName':{'type':'string'},
            
            'ClientUserName':{'type':'string'},
            
            'ClientAgent':{'type':'string'},
            
            'sessionid':{'type':'integer'},
            
            'NATAddress':{'type':'string'},
            
            'FwcAppPath':{'type':'string'},}

        self.firewall = {
                    'schema': schema,
                    'datasource': {'source': 'TMG_FirewallLog'}}
#-------------------------------------------------------
    def test_schema_fields(self):
        """
        Тестируем типы полей
        """
        self.assertEqual(len(schema), len(self.schema)\
                , 'Колличество полей в эталоне и модели не совпадает.')

        for k in schema:
            self.assertEqual(schema[k]['type'], self.schema[k]['type']\
, 'Неверный тип данных в поле - {0}:{1}, должен быть {2}'\
            .format(k, schema[k]['type'], self.schema[k]['type']))

    def test_collection_name(self):
        """
        Тестируем соотвествие названия коллкции эталону
        """
        self.assertEqual(firewall['datasource']['source']\
            , self.firewall['datasource']['source']\
            , 'Название коллекций не совпадает!')

#-------------------------------------------------------
    def tearDown(self): pass


#===============================================================================

if __name__ == "__main__":
    unittest.main()
