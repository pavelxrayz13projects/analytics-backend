#!/usr/bin/env python
# -*- coding: utf-8 -*-
#===============================================================================
"""
    Юнит тест дляТест_для_модельки_tmg_firewall
"""
#===============================================================================
import os, sys
sys.path.append(os.getcwd())

import unittest
import random
from models.tmg_webproxy import schema, webproxy

class  TestWebproxy(unittest.TestCase):
    def setUp(self):
        self.schema = {

            'ClientIP':{'type':'string'},
            
            'ClientUserName':{'type':'string'},
            
            'ClientAgent':{'type':'string'},
            
            'logTime':{'type':'datetime'},
            
            'servername':{'type':'string'},
            
            'referredserver':{'type':'string'},
            
            'DestHost':{'type':'string'},
            
            'DestHostIP':{'type':'string'},
            
            'DestHostPort':{'type':'integer'},
            
            'processingtime':{'type':'integer'},
            
            'bytesrecvd':{'type':'integer'},
            
            'bytessent':{'type':'integer'},
            
            'protocol':{'type':'string'},
            
            'transport':{'type':'string'},
            
            'operation':{'type':'string'},
            
            'uri':{'type':'string'},
            
            'mimetype':{'type':'string'},
            
            'resultcode':{'type':'integer'},
            
            'SrcPort':{'type':'integer'},}

        self.webproxy = {
                    'schema': schema,
                    'datasource': {'source': 'TMG_WebProxyLog'}}
#-------------------------------------------------------
    def test_schema_fields(self):
        """
        Тестируем типы полей
        """
        self.assertEqual(len(schema), len(self.schema)\
                , 'Колличество полей в эталоне и модели не совпадает.')

        for k in schema:
            self.assertEqual(schema[k]['type'], self.schema[k]['type']\
, 'Неверный тип данных в поле - {0}:{1}, должен быть {2}'\
            .format(k, schema[k]['type'], self.schema[k]['type']))

    def test_collection_name(self):
        """
        Тестируем соотвествие названия коллкции эталону
        """
        self.assertEqual(webproxy['datasource']['source']\
            , self.webproxy['datasource']['source']\
            , 'Название коллекций не совпадает!')

#-------------------------------------------------------
    def tearDown(self): pass


#===============================================================================

if __name__ == "__main__":
    unittest.main()
