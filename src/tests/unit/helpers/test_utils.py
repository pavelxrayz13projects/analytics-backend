import unittest
from collections import OrderedDict
from datetime import datetime

from helpers.utils import (create_pcap_url, str_to_date,
                           create_time_range_with_step, get_timestep_key)


class TestUtils(unittest.TestCase):
    def test_create_pcap_url(self):
        self.assertEqual(create_pcap_url('1', '2'), 'http://1/service/pcap_payload?id=2')

    def test_str_to_date(self):
        self.assertTrue(isinstance(str_to_date('2016-09-03T18:17:40.000Z'), datetime))
        self.assertIsNone(str_to_date('2016.09.03T18:17:40'))
        self.assertIsNone(str_to_date(None))

    def test_get_timestep_key(self):
        data = datetime(year=2016, month=5, day=30, hour=17, minute=21, second=55)
        self.assertEqual(get_timestep_key(data), '2016-151-17-21')

    def test_create_time_range_with_step(self):
        starts = datetime(year=2016, month=5, day=30, hour=17, minute=21)
        ends = datetime(year=2016, month=5, day=30, hour=17, minute=25)
        step = 1

        result = OrderedDict([
            ('2016-151-17-21', 0),
            ('2016-151-17-22', 0),
            ('2016-151-17-23', 0),
            ('2016-151-17-24', 0),
            ('2016-151-17-25', 0),
        ])

        self.assertEqual(create_time_range_with_step(starts, ends, step), result)

        starts = datetime(year=2016, month=5, day=30, hour=16, minute=52)
        ends = datetime(year=2016, month=5, day=30, hour=17, minute=35)
        step = 10

        result = OrderedDict([
            ('2016-151-16-50', 0),
            ('2016-151-17-0', 0),
            ('2016-151-17-10', 0),
            ('2016-151-17-20', 0),
            ('2016-151-17-30', 0),
        ])

        self.assertEqual(create_time_range_with_step(starts, ends, step), result)

        starts = datetime(year=2016, month=5, day=30, hour=23, minute=52)
        ends = datetime(year=2016, month=5, day=31, hour=1, minute=5)
        step = 10

        result = OrderedDict([
            ('2016-151-23-50', 0),
            ('2016-152-0-0', 0),
            ('2016-152-0-10', 0),
            ('2016-152-0-20', 0),
            ('2016-152-0-30', 0),
            ('2016-152-0-40', 0),
            ('2016-152-0-50', 0),
            ('2016-152-1-0', 0),
        ])

        self.assertEqual(create_time_range_with_step(starts, ends, step), result)
