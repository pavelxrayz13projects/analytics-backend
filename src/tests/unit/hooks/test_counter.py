import unittest
from unittest.mock import Mock

from werkzeug.exceptions import BadRequest

from hooks.counter import (before_aggregation_counters,
                           before_returning_counters)
from helpers.utils import str_to_date


class TestCounterHooks(unittest.TestCase):
    def test_before_aggregation_counters(self):
        date_from = '2016-04-14T18:17:40.000Z'
        date_to = '2016-04-14T18:17:40.700Z'

        good_request = Mock()
        good_request.aggregation = '{{"$from":"{}","$to":"{}"}}'.format(date_from, date_to)

        empty_request = Mock()
        empty_request.aggregation = None

        bad_request = Mock()
        bad_request.aggregation = '{]'

        partial_request = Mock()
        partial_request.aggregation = '{{"$from":"{}"}}'.format(date_from)

        pipeline = [{'$match': {}}]

        completed_pipeline = [
            {'$match':
                 {'logdate': {
                     '$gte': str_to_date(date_from),
                     '$lte': str_to_date(date_to)}
                 }
             }
        ]

        self.assertEqual(before_aggregation_counters(good_request, pipeline), completed_pipeline)
        self.assertRaises(BadRequest, before_aggregation_counters, partial_request, pipeline)
        self.assertRaises(BadRequest, before_aggregation_counters, bad_request, pipeline)
        self.assertRaises(BadRequest, before_aggregation_counters, empty_request, pipeline)

    def test_before_returning_counters(self):
        response_before = {
            '_items': [
                {'count': 1, '_id': {'group': None}},
                {'count': 1, '_id': {'group': 'test_group'}}
            ]
        }

        response_after = {
            '_items': [
                {'count': 1, 'group': 'no_group'},
                {'count': 1, 'group': 'test_group'}
            ]
        }

        self.assertEqual(before_returning_counters(response_before), response_after)
