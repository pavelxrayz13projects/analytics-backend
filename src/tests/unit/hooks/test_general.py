import unittest
from unittest.mock import patch

from hooks import general


class TestGeneralHooks(unittest.TestCase):
    test_resource = 'test_resource'
    test_choices = {'field': [1, 2, 3]}

    @patch('constants.CHOICES', {test_resource: test_choices})
    def test_add_choices(self):
        empty_response = {'_items': []}
        empty_response_with_choices = {
            '_items': [],
            '_meta': {'choices': self.test_choices}
        }

        response_with_meta = {'_items': [], '_meta': {'my_meta': ''}}
        response_with_meta_with_choices = {
            '_items': [],
            '_meta': {'my_meta': '', 'choices': self.test_choices}
        }

        self.assertEqual(general.add_choices(self.test_resource, empty_response),
                         empty_response_with_choices)
        self.assertEqual(general.add_choices(self.test_resource, response_with_meta),
                         response_with_meta_with_choices)
