"""Тесты для триггеров инцидентов"""
import unittest

from hooks.incidents import replace_pcap_url


class TestIncidentsHooks(unittest.TestCase):
    def test_replace_pcap_url(self):
        self.maxDiff = None

        events_before = {
            "ip": "10.0.14.143",
            "sensor": "IDS",
            "external_id": "1",
            "events": [
                {
                    "src_mac": "e0:cb:4e:fc:83:e7",
                    "event_id": "2",
                    "message": "ET POLICY RDP connection confirm",
                    "dst_address": "93.170.168.27:52213",
                    "pcap_link": "http://10.0.14.143/service/pcap_payload?id=24863478",
                    "src_address": "91.244.183.145:3389",
                    "dst_mac": "00:00:0c:07:ac:01",
                    "date_fact": "2016-10-07T08:52:42.127Z"
                }
            ]
        }

        events_after = {
            "ip": "10.0.14.143",
            "sensor": "IDS",
            "external_id": "1",
            "events": [
                {
                    "src_mac": "e0:cb:4e:fc:83:e7",
                    "event_id": "2",
                    "message": "ET POLICY RDP connection confirm",
                    "dst_address": "93.170.168.27:52213",
                    "pcap_link": "/pcap/1/2",
                    "src_address": "91.244.183.145:3389",
                    "dst_mac": "00:00:0c:07:ac:01",
                    "date_fact": "2016-10-07T08:52:42.127Z"
                }
            ]
        }

        self.assertEqual(replace_pcap_url(events_before), events_after)
